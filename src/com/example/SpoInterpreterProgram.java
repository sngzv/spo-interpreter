package com.example;

import com.example.error.Errors;
import com.example.lexer.Lexer;
import com.example.lexer.Syntax;
import com.example.parser.Parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SpoInterpreterProgram {
    public static void main(String[] args) throws Exception {
        List<String> codeLine = new ArrayList<>();
        try(FileReader reader = new FileReader("src/com/example/input.txt");
            BufferedReader br = new BufferedReader(reader)) {
            while (br.ready()) {
                codeLine.add(br.readLine().replaceAll("\\s{2,}", " ").trim());
            }
        } catch (IOException e) {
            System.out.println("Произошла ошибка при чтении файла!");
        }

        Lexer lexer = new Lexer(codeLine);
        Syntax syntax = new Syntax(lexer);
        if (!Errors.isErrors()) {
            Parser interpreter = new Parser(syntax.getPoliz());
            interpreter.start();
        }
    }
}