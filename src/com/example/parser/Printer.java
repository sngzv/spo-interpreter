package com.example.parser;

// Класс вывода в консоль.
public class Printer {
    public static class console {

        public static void write(String msg) {
            System.out.println(msg);
        }
    }
}
