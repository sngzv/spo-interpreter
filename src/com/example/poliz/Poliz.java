package com.example.poliz;

import java.util.Stack;

// Формирования ПОЛИЗ.
public class Poliz {

    private Stack<String> poliz;

    public Poliz() {
        poliz = new Stack<>();
    }

    public void toPoliz(String token) {
        poliz.push(token + " - " + (poliz.size() + 1));
    }

    public String get(int index) {
        return poliz.get(index);
    }

    public void replace(int index, String token) {
        poliz.remove(index);
        poliz.add(index, token);
    }

    public int find(String token) {
        for (int i = 0; i < getSize(); i++)
            if (poliz.contains(token + " - " + i))
                return i;
        return -1;
    }

    public int getSize() {
        return poliz.size() + 1;
    }

    public String[] getAll() {
        String[] all = new String[poliz.size()];
        for (int i = 0; i < poliz.size(); i++) {
            all[i] = poliz.get(i);
        }
        return all;
    }

    public void remove(int index) {
        poliz.remove(index);
    }

}