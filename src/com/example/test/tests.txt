Файл с примера программ.

program main(){
    int b = 34;
    int c = 5;
    int r = c + (b - 17) * (20 / 10);
    print("Result:");
    print(r);
}

program main(){
    for (int i = 0; i < 10; i = i + 1){
        print(\"Hello, world!");
    }
}

program main(){
    int a = 10;
    if (20 > a) {
        print("20 > a");
    }
}

program main(){
    int a = 18;
    if (20 > a) {
        print("20 > a");
    } else {
        print("20 < a");
    }
}

program main(){
    int a = 10;
    if (a > 5) {
        for (int i = 1; i < a; i = i + 1){
            print(\"Hello, world!");
        }
    } else {
        for (int i = 1; i < 5; i = i + 1){
            print(\"Hello, world!");
        }
    }
}

program main(){
    int a = 0;
    int b = 5;
    int r = a + b;
    double c = 10.14;
    double d = 11.25;
    double f = c + d;
    string D = \" Переменная String!\";
    string E = D + \"Переменная String 2!\";
    print(f);
}