package com.example.lexer;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

// Лексер.
public class Lexer {

    private final String[] separators = {",", ";", "{", "}", "(", ")", "\""};
    private final String[] operators = {"+", "-", "*", "/", "=", "!", ">", "<", "==", "<=", ">=", "!=", "||", "&&", ":"};
    private final String[] keyWords = {"if", "else", "for", "goto", "cin", "print", "main", "return"};
    private final String[] comments = {"/*", "*/", "//"};
    private final String[] types = {"int", "double", "string"};

    // Счетчик токенов.
    private int k = 0; //key words
    private int s = 0; //separator
    private int i = 0; //identifier
    private int l = 0; //string
    private int n = 0; //int
    private int d = 0; //double
    private int o = 0; //operator

    // Стэки, хранящие в себе лексические токены, позиции токенов, текст и номера линий
    public Stack<String> letter;
    public Stack<Integer> position;
    public Stack<String> text;
    public Stack<Integer> line;

    private List<String> tokens = new ArrayList<>();

    private int LineId = 0;

    public Lexer(List<String> program) {
        letter = new Stack<>();
        position = new Stack<>();
        text = new Stack<>();
        line = new Stack<>();

        /*
         * Разбиваем переданный текст на массив линий
         */
        String[] lines = program.toArray(new String[0]);

        String buffer = "";
        char[] tempLines;
        boolean isStr = false;
        boolean isComm = false;

        // Проход по всем строкам кода.
        for (int i = 0; i < lines.length; i++) {

            tempLines = lines[i].toCharArray();
            LineId = i;

            for (int j = 0; j < lines[i].length(); j++) {
                second:
                {
                    char second;
                    char pastChar = ' ';

                    if (String.valueOf(tempLines[j]).matches("\\d|\\w|[.]"))
                        buffer += tempLines[j];
                    else {

                        if (j + 1 >= tempLines.length)
                            second = ' ';
                        else
                            second = tempLines[j + 1];

                        if (j - 1 >= 0)
                            pastChar = tempLines[j - 1];


                        while (isStr) {
                            if (tempLines[j] != '"') {
                                buffer += tempLines[j];
                                break second;
                            } else {
                                isStr = false;
                                checkOther(buffer);

                                tokens.add("[IDENTIFIER] - " + buffer);

                                if (checkSeparators(String.valueOf(tempLines[j]))) {
                                    tokens.add("[SEPARATOR] - " + tempLines[j]);
                                }

                                buffer = "";
                                break second;
                            }
                        }
                        int comm = checkComments(tempLines[j], second);


                        if (comm == 1) {
                            tokens.add("[COMMENT LINE] - " + tempLines[j] + second);
                            break;
                        } else if (comm == 2) {
                            tokens.add("[COMMENT START] - " + tempLines[j] + second);
                            buffer = "";
                            j += 2;
                            isComm = true;
                        } else if (comm == 3) {
                            tokens.add("[COMMENT END] - " + tempLines[j] + second);
                            j += 2;
                            isComm = false;
                            buffer = "";
                            break second;
                        }


                        if (isComm)
                            break second;

                        int type = checkType(buffer);

                        if (checkKeyWords(buffer)) {
                            tokens.add("[KEYWORD] - " + buffer);
                        } else if (type == 1) {
                            tokens.add("[INTEGER] - " + buffer);
                        } else if (type == 2) {
                            tokens.add("[DOUBLE] - " + buffer);
                        } else if (type == 3) {
                            tokens.add("[STRING] - " + buffer);
                        } else if (checkOther(buffer) == 2) {
                            tokens.add("[IDENTIFIER] - " + buffer);
                        }

                        int oper = checkOperators(tempLines[j], second, pastChar);

                        if (oper == 2) {
                            tokens.add("[OPERATOR] - " + tempLines[j] + second);
                            buffer = "";
                            break second;
                        } else if (oper == 1) {
                            tokens.add("[OPERATOR] - " + tempLines[j]);
                        } else if (checkSeparators(String.valueOf(tempLines[j]))) {
                            tokens.add("[SEPARATOR] - " + tempLines[j]);
                        }

                        if (tempLines[j] == '"')
                            isStr = true;

                        buffer = "";
                    }

                }
            }

        }
    }

    // Проверка являится ли слово ключевым.
    private boolean checkKeyWords(String word) {
        for (String keyWord : keyWords)
            if (word.equals(keyWord)) {
                letter.push("k");
                position.push(k);
                k++;
                text.push(word);
                line.push(LineId);
                return true;
            }

        return false;
    }

    // Метод, проверяющий является ли слово разделителем
    private boolean checkSeparators(String word) {
        for (String separator : separators)
            if (word.equals(separator)) {
                letter.push("s");
                position.push(s);
                s++;
                text.push(word);
                line.push(LineId);
                return true;
            }

        return false;
    }

    // Метод, проверяющий является ли слово типом данных и каким именно, если является
    private int checkType(String word) {
        if (word.equals(types[0])) {
            letter.push("n");
            position.push(n);
            n++;
            text.push(word);
            line.push(LineId);
            return 1;
        } else if (word.equals(types[1])) {
            letter.push("d");
            position.push(d);
            d++;
            text.push(word);
            line.push(LineId);
            return 2;
        } else if (word.equals(types[2])) {
            letter.push("l");
            position.push(l);
            l++;
            text.push(word);
            line.push(LineId);
            return 3;
        }

        return 0;
    }

    // Метод, проверяющий являетя ли слово оператором
    private int checkOperators(char word, char nextChar, char pastChar) {
        for (String operator : operators)
            if (operator.length() == 2)
                if (word == operator.charAt(0) && nextChar == operator.charAt(1)) {
                    letter.push("o");
                    position.push(o);
                    text.push(operator);
                    line.push(LineId);
                    o++;

                    return 2;
                }

        for (String operator : operators)
            if (String.valueOf(word).equals(operator) && pastChar != '=' && pastChar != '>' && pastChar != '<' && pastChar != '!') {
                letter.push("o");
                position.push(o);
                text.push(operator);
                line.push(LineId);
                o++;
                return 1;
            }

        return 0;
    }

    // Проверяет является ли слово комментарием.
    private int checkComments(char word, char nextChar) {
        if (word == comments[2].charAt(0) && nextChar == comments[2].charAt(1))
            return 1;
        else if (word == comments[0].charAt(0) && nextChar == comments[0].charAt(1))
            return 2;
        else if (word == comments[1].charAt(0) && nextChar == comments[1].charAt(1))
            return 3;

        return 0;
    }

    // Проверка на все остальное.
    private int checkOther(String word) {
        if (word.length() > 0) {
            letter.push("i");
            position.push(i);
            i++;
            text.push(word);
            line.push(LineId);
            return 2;
        }
        return 0;
    }

}