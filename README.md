**Интерпретатор**

Простой интерпретатор с возможностью обрабатывать логические условия if/else, цикл for, вывод текста в консоль, вычисление математических выражений с неограниченным числом вложенных скобок. 
Ввод исходного кода осуществляется в файл, который читает программа и результат вывод в консоль. Так же есть некоторая проверка синтаксиса, в случае нарушение которого, будет выведена соответсвующая ошибка. 

Примеры работы компилятора:

1)
![Alt-текст](https://sun9-4.userapi.com/impg/1SPjMHYcS2NcEYz-IgXbYblXIIMzov1pv_rwTg/1abO4WufHI0.jpg?size=583x976&quality=96&sign=06236f5faf0ac01a18242ba2cc327e66&type=album)

2)
![Alt-текст](https://sun9-1.userapi.com/impg/iMYO89lJSxVxlOGq0s7WGtW3MyVIBL_2cLEGCQ/vQu3Q8EZ0Js.jpg?size=691x551&quality=96&sign=65c18e981d672af4880664808ba1fb39&type=album)

3)
![Alt-текст](https://sun9-24.userapi.com/impg/uIMthm2O0L95rpHG0NOeJnr6xTGqtY6PCLmFgg/7RRYzYLHjgM.jpg?size=467x683&quality=96&sign=5b3cc78c88943f70cae3e17ac6726a98&type=album)

4)
![Alt-текст](https://sun9-28.userapi.com/impg/4a7ZF4P7Rr7GEN3IaICwYo-ayCZMhaPKxKCC8A/jhowohCEHqM.jpg?size=458x748&quality=96&sign=5d0c58742b1263c1f7b04595a499917e&type=album)

5)
![Alt-текст](https://sun9-51.userapi.com/impg/4plTyRl9dNngzdDDi4EJWq2uLbSPwSPaarCOWw/uaSu_bv9rS4.jpg?size=429x665&quality=96&sign=82092ceb551889543cb6e97ec80764c1&type=album)

6)
![Alt-текст](https://sun9-9.userapi.com/impg/0niU5eflsG0vmwJW8H-GcF76xcXxsqPIFL7NaQ/-ok-KkFKFxQ.jpg?size=414x431&quality=96&sign=e49479b0c8954f10aec735bd583e3dc8&type=album)

7)
![Alt-текст](https://sun9-27.userapi.com/impg/kdWGE0Rbz1pyUxD2Ul2SfEjQlus2SJQKO6Mw2g/brOt4YqfDOY.jpg?size=511x588&quality=96&sign=f1c11f4bf56c5fcc3c0a6fa64ab7e354&type=album)

8)
![Alt-текст](https://sun9-20.userapi.com/impg/lz-XDyw-qFvb8_piepnvvwFEDGHCAKfcrHu7Kw/eprycup-gYI.jpg?size=679x527&quality=96&sign=eb95965a55b8ed0c0d5711e2a28bc42d&type=album)

Так же в компиляторе есть своя реализация связного списка и хеш-таблицы.
